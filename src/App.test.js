import { screen, render, waitFor } from "@testing-library/react";
import App from "./App";

//describe("regex search", () => {
//test("app.js testing process", () => {
//  render(<App></App>);
//  const element = screen.getByText(/testing react app/i);
//  expect(element).toBeInTheDocument();
//});
//});

//describe("exact match", () => {
//  test("app.js testing process", () => {
//    render(<App></App>);
//    const element = screen.getByText("testing react app", {
//      exact: false,
//    });
//    expect(element).toBeInTheDocument();
//  });
//});

//getByText --> have control
//getByText --> haven't control

//test("app.js testing", () => {
//  render(<App />);
//  const element = screen.queryByText("testing react app");
//  expect(element).not.toBeInTheDocument();
//});

//findByText --> async await

//test("app.js testing", async () => {
//  render(<App />);
//  const element = await screen.findByText("xxx");
//  expect(element).toBeInTheDocument();
//});

//test("app.js testing", async () => {
//  render(<App />);
//  let element;
//  await waitFor(() => {
//    element = screen.getByText("qqq");
//  });
//  expect(element).toBeInTheDocument();
//});

//test("app.js testing", () => {
//  render(<App />);
//  const element = screen.getByLabelText("React");
//  expect(element).toHaveValue("Test");
//});

//test("app.js testing", () => {
//  render(<App />);
//  const element = screen.getByTitle("test");
//  expect(element).toBeInTheDocument();
//});

//test("app.js testing", () => {
//  render(<App />);
//  const element = screen.getAllByRole("button");
//  expect(element[0]).toBeInTheDocument();
//});

//test("app.js getByDisplayValue", () => {
//  render(<App />);
//  const element = screen.getByDisplayValue("testing");
//  expect(element).toBeInTheDocument();
//});

//test("app.js getByTestId", () => {
//  render(<App />);
//  const element = screen.getByTestId("test");
//  expect(element).toBeInTheDocument();
//});

//test("app.js a tag href toContain", () => {
//  render(<App />);
//  const element = screen.getByText("Link");
//  expect(element.href).toContain("http://");
//});

//test("app.js getByAltText", () => {
//  render(<App />);
//  const element = screen.getByAltText("image");
//  expect(element).toBeInTheDocument();
//});
test("app.js getByRole input", () => {
  render(<App />);
  const element = screen.getByRole("textbox");
  expect(element).toBeInTheDocument();
});
