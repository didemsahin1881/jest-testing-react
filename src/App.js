import logo from "./logo.svg";
import "./App.css";
import { useEffect, useState } from "react";

function App() {
  //const name = "Didem";
  //return <div className="App">testing react app {name}</div>;

  // const [name, setName] = useState("xxx");
  // useEffect(() => {
  //   setTimeout(() => {
  //     setName("qqq");
  //   }, 300);
  // });

  return (
    <div className="App">
      <label htmlFor="input">React</label>
      <input id="input" defaultValue="Test"></input>
      <div title="test">test</div>
      <div title="test1">test</div>
      <button>button</button>
      <button>button2</button>
      <h1>heading role </h1>
      <a href="http://google.com">Link</a>
      <img src="" alt="image"></img>
    </div>
  );
}

export default App;
