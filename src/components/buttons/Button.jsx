import React from 'react'
import { useState } from "react";

const Button = () => {
    const [count, setCount] = useState(0);
    return (
        <div>
            <h1>{count}</h1>
            <button onClick={() => setCount(count + 1)}>btn user</button>
        </div>
    );
}

export default Button
