import { screen, render } from "@testing-library/react";
import Button from "./Button";
import userEvent from "@testing-library/user-event";
//test("Button.js ", () => {
//  render(<Button />);
//  const element = screen.getByText("Button");
//  expect(element).toBeInTheDocument();
//});
//it("Button.js toBe", () => {
//  render(<Button />);
//  expect(2 + 2).toBe(4);
//});
//
//it("Button.js toEqual", () => {
//  render(<Button />);
//  let obj = {
//    one: 1,
//  };
//  obj["two"] = 2;
//  expect(obj).toEqual({ one: 1, two: 2 });
//});

test("Button.js userEvent", async () => {
  const user = userEvent.setup();
  render(<Button />);
  screen.debug();
  await user.pointer({
    keys: "[MouseLeft][MouseRight]",
    target: screen.getByRole("button"),
  });
  expect(screen.getByRole("heading")).toHaveTextContent(1);
});
test("Button.js container", async () => {
  const user = userEvent.setup();
  const { container } = render(<Button />);
  console.log(container);
  expect(screen.getByRole("heading")).toHaveTextContent("1");
});
